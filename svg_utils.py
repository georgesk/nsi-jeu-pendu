"""
Quelques utilitaires en Python pour digérer des fichiers SVG
"""
from xml.dom import minidom

def lireSvg(nomFichier: str) -> minidom.Document:
    """
    lecture d'un fichier SVG
    @param nomFichier le nom d'un fichier
    @return un objet de type minidom.Document
    """
    svgdoc=None
    with open(nomFichier) as svgfile:
        svgdoc = minidom.parse(svgfile)
    assert (type(svgdoc) is minidom.Document)
    return svgdoc

le_prefixe = "i" # un préfixe pour des identifiants d'éléments utiles

def bonPrefixe(xml, prefixe):
    """
    détermine si oui ou non l'identifiant d'un élément est acceptable
    """
    # nodeType == 1 correspond au type ELEMENT_NODE
    return xml.nodeType == 1 and \
        xml.getAttribute("id").startswith(prefixe)

def bonId(xml, ident):
    """
    détermine si oui ou non l'identifiant est le même
    """
    # nodeType == 1 correspond au type ELEMENT_NODE
    return xml.nodeType == 1 and \
        xml.getAttribute("id") == ident
      
def tousElements(xml, prefixe, accumulateur = {}):
    """
    renvoie un dictionnaire de tous les éléments situés sous un
    élément xml donné dont l'id commence par un préfixe donné ;
    les entrées du dictionnaire sont id -> element
    @param xml : un objet de type minidom.Node
    @param préfixe qui détermine si on doit le mettre dans l'accumulateur
    """
    for c in xml.childNodes:
        if bonPrefixe(c, le_prefixe):
            accumulateur[c.getAttribute("id")] = c
        tousElements(c, le_prefixe, accumulateur)
    return accumulateur
