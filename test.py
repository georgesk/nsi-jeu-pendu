#! /usr/bin/env python3

from ui_main import Ui_MainWindow
from svg_utils import *

from PyQt5.QtWidgets import QApplication, QMainWindow, \
    QVBoxLayout, QHBoxLayout, QCheckBox
from PyQt5.QtSvg import QSvgWidget
import sys

    

class MaFenetre(QMainWindow, Ui_MainWindow):
    def __init__(self, dessin="dessin.svg", parent=None):
        QMainWindow.__init__(self, parent)
        self.dessin = dessin
        self.setupUi(self)
        # on récupère la structure du dessin
        self.svgdoc= lireSvg(self.dessin)
        # on repère les éléments utiles du dessin
        self.tous = tousElements(self.svgdoc.documentElement, le_prefixe)
        self.ids = sorted(self.tous.keys())
        # puis on complète l'interface graphique
        self.boutonsEtDessin()
        return

    def boutonsEtDessin(self):
        
        # on fait une case à cocher pour chaque élément, d'après son id
        # ça a du sens pour un test ;
        # évidemment pour le jeu du pendu, de telles cases sont inutiles
        layout1 = QHBoxLayout()
        self.groupBox.setLayout(layout1)
        for ident in self.ids:
            b = QCheckBox(ident)
            b.setCheckState(2)
            b.stateChanged.connect(self.fabriqueMethode(ident))
            layout1.addWidget(b)
            
        # on crée le widget svg puis on y trace le dessin
        self.svgWidget = QSvgWidget(self.svgFrame)
        layout2 = QVBoxLayout()
        self.svgFrame.setLayout(layout2)
        layout2.addWidget(self.svgWidget)
        self.dessine()
        return

    def dessine(self):
        """
        force le rendu du tracé SVG contenu dans self.svgdoc, dans le
        widget du haut
        """
        self.svgWidget.load(self.svgdoc.toxml().encode("utf-8"))
        return

    def fabriqueMethode(self, ident):
        """
        fabrique une fonction de rappel qui prend le code de changement
        d'état d'une case à cocher et s'applique à l'élémént qui a le bon id
        """
        def bascule(etat):
            element = self.tous[ident]
            if etat == 0: # il faut cacher l'élément
                element.setAttribute("display", "none")
            else: # on doit montrer l'élément
                element.setAttribute("display", "inline")
            self.dessine()
            return
        return bascule



if __name__ == "__main__":
    app = QApplication (sys.argv)
    mf = MaFenetre("dessin2.svg")
    mf.show()
    result = app.exec_()
    sys.exit(result)
