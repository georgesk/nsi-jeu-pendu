UI_sources = $(shell ls *.ui)
UI_pyfiles = $(patsubst %.ui, ui_%.py, $(UI_sources))

all: $(UI_pyfiles)

ui_%.py : %.ui
	pyuic5 $< -o $@

.PHONY: all
